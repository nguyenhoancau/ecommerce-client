import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule) },
  { path: 'edit', loadChildren: () => import('./edit-product/edit-product.module').then(m => m.EditProductModule) },
  { path: 'edit/:id', loadChildren: () => import('./edit-product/edit-product.module').then(m => m.EditProductModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
