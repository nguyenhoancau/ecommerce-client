export class Product {
    _id?: string;
    title: string;
    status: string;
    description: string;
    price: number;

    constructor() {
        this._id = '';
        this.title = '';
        this.status = '';
        this.description = '';
        this.price = 0;
    }
}