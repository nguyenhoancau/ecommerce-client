import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './product';
import { ProductsService } from './products.service';
import { HttpResponse } from '@angular/common/http';
import { ProductBiz } from '../biz.models/product.biz';
import { DeleteRequested, DeleteConfirmed, DeleteFailed, DeleteCanceled } from '../biz.models/product-state-machine.event';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];

  constructor(
    private productService: ProductsService,
    private productBiz: ProductBiz,
    ) { }

  ngOnInit() {
    this.productBiz.initStateMachine();
    this.getData();
  }

  getData() {
    this.productBiz.fetchData().subscribe(res => this.products = [ ...res.body]);
  }

  getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return { background: color};
  }

  onDelete(id) {
    this.productBiz.deleteProduct(id).subscribe(res => {
      if (res) {
        this.productBiz.transition(new DeleteConfirmed(id));
        this.getData();
      }
      else {
        this.productBiz.transition(new DeleteCanceled());
      }
    });

    // this.productService.deleteProduct(id).subscribe(data => {
    //   this.getData();
    //   console.log('Quote Deleted');
    // });
  }

}
