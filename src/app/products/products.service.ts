import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Product } from './product';
import { environment } from 'src/environments/environment';

const baseUrl = environment.baseUrl;

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private http: HttpClient) {}

  getDatas(): Observable<HttpResponse<Product[]>> {
    return this.http.get<Product[]>(`${baseUrl}/products`, { observe: 'response' }).pipe(
      tap(data => console.log(data))
    );
  }

  getData(id: string): Observable<Product> {
    return this.http
      .get<Product>(`${baseUrl}/products/${id}`)
      .pipe(tap((data) => console.log('Product:', data)));
  }

  CreateProduct(product: Product): Observable<HttpResponse<Product>> {
    return this.http.post<HttpResponse<Product>>(
      `${baseUrl}/products`,
      product
    );
  }

  updateProduct(
    id: string,
    product: Product
  ): Observable<HttpResponse<Product>> {
    return this.http.put<HttpResponse<Product>>(
      `${baseUrl}/products/${id}`,
      product
    );
  }

  deleteProduct(id: string): Observable<HttpResponse<Product>> {
    return this.http.delete<HttpResponse<Product>>(`${baseUrl}/products/${id}`);
  }
}
