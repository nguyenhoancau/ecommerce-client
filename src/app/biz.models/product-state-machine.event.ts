import { EventObject } from 'xstate';
import { Product } from '../products/product';

export class FetchSucceed implements EventObject {
    static readonly type = '[PRODUCTS] FETCH SUCCEED';
    readonly type = FetchSucceed.type;
    constructor(
        public products: Product[],
        public message: string
    ) { }
}

export class FetchFailed implements EventObject {
    static readonly type = '[PRODUCTS] FETCH FAILED';
    readonly type = FetchFailed.type;
    constructor(public message: string) { }
}

export class CreateRequested implements EventObject {
    static readonly type = '[PRODUCTS] CREATE REQUESTED';
    readonly type = CreateRequested.type;
    constructor() { }
}

export class CreateCanceled implements EventObject {
    static readonly type = '[PRODUCTS] CREATE CANCELED';
    readonly type = CreateCanceled.type;
    constructor() { }
}

export class CreateConfirmed implements EventObject {
    static readonly type = '[PRODUCTS] CREATE CONFIRMED';
    readonly type = CreateConfirmed.type;
    constructor(public product: Product) { }
}

export class CreateSucceed implements EventObject {
    static readonly type = '[PRODUCTS] CREATE SUCCEED';
    readonly type = CreateSucceed.type;
    constructor(
        public product: Product,
        public message: string
    ) { }
}

export class CreateFailed implements EventObject {
    static readonly type = '[PRODUCTS] CREATE FAILED';
    readonly type = CreateFailed.type;
    constructor(public message: string) { }
}

export class UpdateRequested implements EventObject {
    static readonly type = '[PRODUCTS] UPDATE REQUESTED';
    readonly type = UpdateRequested.type;
    constructor() { }
}

export class UpdateCanceled implements EventObject {
    static readonly type = '[PRODUCTS] UPDATE CANCELED';
    readonly type = UpdateCanceled.type;
    constructor() { }
}

export class UpdateConfirmed implements EventObject {
    static readonly type = '[PRODUCTS] UPDATE CONFIRMED';
    readonly type = UpdateConfirmed.type;
    constructor(
        public product: Product,
        public id: string
    ) { }
}

export class UpdateSucceed implements EventObject {
    static readonly type = '[PRODUCTS] UPDATE SUCCEED';
    readonly type = UpdateSucceed.type;
    constructor(
        public product: Product,
        public id: string,
        public message: string
    ) { }
}

export class UpdateFailed implements EventObject {
    static readonly type = '[PRODUCTS] UPDATE FAILED';
    readonly type = UpdateFailed.type;
    constructor(public message: string) { }
}

export class DeleteRequested implements EventObject {
    static readonly type = '[PRODUCTS] DELETE REQUESTED';
    readonly type = DeleteRequested.type;
    constructor() { }
}

export class DeleteCanceled implements EventObject {
    static readonly type = '[PRODUCTS] DELETE CANCELED';
    readonly type = DeleteCanceled.type;
    constructor() { }
}

export class DeleteConfirmed implements EventObject {
    static readonly type = '[PRODUCTS] DELETE CONFIRMED';
    readonly type = DeleteConfirmed.type;
    constructor(public id: string) { }
}

export class DeleteSucceed implements EventObject {
    static readonly type = '[PRODUCTS] DELETE SUCCEED';
    readonly type = DeleteSucceed.type;
    constructor(
        public id: string,
        public message: string
    ) { }
}

export class DeleteFailed implements EventObject {
    static readonly type = '[PRODUCTS] DELETE FAILED';
    readonly type = DeleteFailed.type;
    constructor(public message: string) { }
}

export type ProductStateMachineEvent =
    FetchSucceed | FetchFailed |
    CreateRequested | CreateCanceled | CreateConfirmed | CreateSucceed | CreateFailed |
    UpdateRequested | UpdateCanceled | UpdateConfirmed | UpdateSucceed | UpdateFailed |
    DeleteRequested | DeleteCanceled | DeleteConfirmed | DeleteSucceed | DeleteFailed;
