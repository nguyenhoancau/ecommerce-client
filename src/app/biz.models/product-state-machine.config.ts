import { Product } from '../products/product';
import {
    ProductStateMachineEvent,
    FetchSucceed,
    FetchFailed,
    CreateRequested,
    CreateCanceled,
    CreateConfirmed,
    CreateSucceed,
    CreateFailed,
    UpdateRequested,
    UpdateCanceled,
    UpdateConfirmed,
    UpdateSucceed,
    UpdateFailed,
    DeleteRequested,
    DeleteCanceled,
    DeleteConfirmed,
    DeleteSucceed,
    DeleteFailed
} from './product-state-machine.event';
import { MachineConfig } from 'xstate';

export interface ProductStateMachineSchema {
    states: {
        idle: {},
        fetching: {},
        showing: {},
        create: {},
        creating_process: {},
        update: {},
        updating_process: {},
        delete: {},
        deleting_process: {},
        errors: {}
    };
}

export interface ProductStateMachineContext {
    currentStatus: string;
    products: Product[];
}

const context: ProductStateMachineContext = {
    currentStatus: '',
    products: []
};

export const ProductStateMachineConfig: MachineConfig<
    ProductStateMachineContext,
    ProductStateMachineSchema,
    ProductStateMachineEvent
> = {
    id: 'product_state_machine',
    context,
    initial: 'idle',
    states: {
        idle: {
            on: {
                '': 'fetching'
            }
        },
        fetching: {
            invoke: {
                id: 'fetchProducts',
                src: 'fetchProducts'
            },
            on: {
                [FetchSucceed.type]: {
                    target: 'showing',
                    //cond: 'ACTIVE'
                },
                [FetchFailed.type]: {
                    target: 'errors',
                    //cond: 'DRAFT' || 'DEACTIVE'
                }
            },
        },
        showing: {
            on: {
                [CreateRequested.type]: 'create',
                [UpdateRequested.type]: 'update',
                [DeleteRequested.type]: 'delete'
            }
        },
        create: {
            on: {
                [CreateCanceled.type]: 'showing',
                [CreateConfirmed.type]: 'creating_process'
            }
        },
        creating_process: {
            invoke: {
                id: 'createProduct',
                src: 'createProduct'
            },
            on: {
                [CreateSucceed.type]: {
                    target: 'showing',
                    //cond: 'ACTIVE'
                },
                [CreateFailed.type]: {
                    target: 'showing',
                    //cond: 'DRAFT' || 'DEACTIVE'
                }
            }
        },
        update: {
            on: {
                [UpdateCanceled.type]: 'showing',
                [UpdateConfirmed.type]: 'updating_process'
            }
        },
        updating_process: {
            invoke: {
                id: 'updateProduct',
                src: 'updateProduct'
            },
            on: {
                [UpdateSucceed.type]: {
                    target: 'showing',
                    //cond: 'ACTIVE'
                },
                [UpdateFailed.type]: {
                    target: 'showing',
                    //cond: 'DRAFT' || 'DEACTIVE'
                }
            }
        },
        delete: {
            on: {
                [DeleteCanceled.type]: 'showing',
                [DeleteConfirmed.type]: 'deleting_process'
            }
        },
        deleting_process: {
            invoke: {
                id: 'deleteProduct',
                src: 'deleteProduct'
            },
            on: {
                [DeleteSucceed.type]: {
                    target: 'showing',
                    cond: 'ACTIVE'
                },
                [DeleteFailed.type]: {
                    target: 'showing',
                    cond: 'DRAFT' || 'DEACTIVE'
                }
            }
        },
        errors: {
            type: 'final'
        }
    }
};
