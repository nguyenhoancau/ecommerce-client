import {
    ProductStateMachineEvent,
    FetchSucceed,
    FetchFailed,
    CreateRequested,
    CreateCanceled,
    CreateConfirmed,
    CreateSucceed,
    CreateFailed,
    UpdateRequested,
    UpdateCanceled,
    UpdateConfirmed,
    UpdateSucceed,
    UpdateFailed,
    DeleteRequested,
    DeleteCanceled,
    DeleteConfirmed,
    DeleteSucceed,
    DeleteFailed
} from './product-state-machine.event';
import { Injectable } from '@angular/core';
import { MachineOptions, Machine, interpret, EventObject } from 'xstate';
import { ProductStateMachineContext, ProductStateMachineSchema, ProductStateMachineConfig } from './product-state-machine.config';
import { ProductsService } from '../products/products.service';
import { map, catchError } from 'rxjs/operators';
import { of, from, fromEventPattern, Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { Product } from '../products/product';

@Injectable()
export class ProductBiz {
    cnt = 1;
    public service;
    private machine;
    private productStateMachineOptions: Partial<MachineOptions<
        ProductStateMachineContext,
        ProductStateMachineEvent
    >> = {
        guards: {
            DRAFT: (context, _) => context.currentStatus === 'draft',
            ACTIVE: (context, _) => context.currentStatus === 'active',
            DEACTIVE: (context, _) => context.currentStatus === 'deactive'
        },
        services: {
            fetchProducts: () => this.productService.getDatas().pipe(
                map(res => {
                    if (res.status === 200) {
                        const msg = 'Fetched successfully!';
                        return new FetchSucceed(res.body, msg);
                    } else {
                        const msg = 'Failed to fetch!';
                        return new FetchFailed(msg);
                    }
                }, catchError(() => {
                    const msg = 'Failed to fetch!';
                    return of(new FetchFailed(msg));
                }))
            ),
            createProduct: (_, event: CreateConfirmed) => (
                this.productService.CreateProduct(event.product).pipe(
                    map(res => {
                        if (res.status === 200) {
                            const msg = 'Created succesfully!';
                            return new CreateSucceed(res.body, msg);
                        } else {
                            const msg = 'Failed to create!';
                            return new CreateFailed(msg);
                        }
                    }, catchError((err) => {
                        const msg = 'Failed to create!';
                        return of(new CreateFailed(msg));
                    }))
                )
            ),
            updateProduct: (_, event: UpdateConfirmed) => (
                this.productService.updateProduct(event.id, event.product).pipe(
                    map(res => {
                        if (res.status === 200) {
                            const msg = 'Updated successfully!';
                            return new UpdateSucceed(res.body, res.body._id, msg);
                        } else {
                            const msg = 'Failed to update!';
                            return new UpdateFailed(msg);
                        }
                    }, catchError(() => {
                        const msg = 'Failed to update!';
                        return of(new UpdateFailed(msg));
                    }))
                )
            ),
            deleteProduct: (_, event: DeleteConfirmed) => (
                this.productService.deleteProduct(event.id).pipe(
                    map(res => {
                        if (res.status === 200) {
                            const msg = 'Deleted successfully!';
                            return new DeleteSucceed(event.id, msg);
                        } else {
                            const msg = 'Failed to delete!';
                            return new DeleteFailed(msg);
                        }
                    }, catchError(() => {
                        const msg = 'Failed to delete!';
                        return of(new DeleteFailed(msg));
                    }))
                )
            )
        }
    };
    constructor(
        private productService: ProductsService,
    ) { }

    initStateMachine(): void {
        this.machine = Machine<any, ProductStateMachineSchema, ProductStateMachineEvent>(
            ProductStateMachineConfig
        ).withConfig(this.productStateMachineOptions);

        this.service = interpret(this.machine).start();
        const state$ = from(this.service);
        state$.subscribe((state: any) => {
            console.log(`---------------------`);
            console.log(`State changed the ${this.cnt++} time!`);
            if (!!state.transitions[0]) {
                console.log(`Event: ${state.transitions[0].event} has just been sent!`);
            }
            console.log(`Current state: ${state.value}`);
            console.log(`Available events: ${state.nextEvents}`);
            console.log(state);
            console.log(`---------------------`);
        });
    }

    stopMachine(): void {
        this.service.stop();
    }

    transition(event: ProductStateMachineEvent) {
        this.service.send(event);
    }

    fetchData(): Observable<HttpResponse<Product[]>> {
        return this.productService.getDatas();
    }

    deleteProduct(id: string): Observable<HttpResponse<Product>> {
        return this.productService.deleteProduct(id);
    }
}
