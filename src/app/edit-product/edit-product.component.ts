import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductBiz } from '../biz.models/product.biz';
import { UpdateRequested, CreateRequested, CreateCanceled, UpdateCanceled, UpdateConfirmed, CreateConfirmed } from '../biz.models/product-state-machine.event';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  product = {
    title: '',
    description: '',
    price: 0
  };
  id: string;

  constructor(
      private productService: ProductsService,
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private productBiz: ProductBiz) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.id);
    if (this.id) {
      this.productBiz.transition(new UpdateRequested());
      this.productService.getData(this.id).subscribe(data => {
        this.product = data;
      });
    }
    else {
      this.productBiz.transition(new CreateRequested());
    }
  }

  onSave(form) {
    console.log(form);
    const data = form.value;
    // if ( this.id) {
    //   this.productService.updateProduct(this.id, data).subscribe(product => {
    //     console.log(product);
    //     this.router.navigateByUrl('/products');
    //   });
    // } else
    // {
    //   this.productService.CreateProduct(data).subscribe(product => {
    //     console.log(product);
    //     this.router.navigateByUrl('/products');
    //   });
    // }
    if ( this.id) {
      this.productBiz.transition(new UpdateConfirmed(data, this.id));
      this.router.navigateByUrl('/products');
    } else
    {
      this.productBiz.transition(new CreateConfirmed(data));
      this.router.navigateByUrl('/products');
    }
  }

  onCancel() {
    if (this.id) {
      this.productBiz.transition(new UpdateCanceled());
      this.router.navigateByUrl('/');
    } else {
      this.productBiz.transition(new CreateCanceled());
      this.router.navigateByUrl('/');
    }
  }

}
