export interface EditProduct {
    title: string;
    description: string;
    price: number;
}
